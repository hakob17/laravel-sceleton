<div class="page-sidebar-wrapper">      
    <div class="page-sidebar navbar-collapse collapse">
       <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li {{ (Request::is('dashboard') ? 'class=active' : '') }}>
                <a href="/dashboard">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li {{ (Request::is('users') ? 'class=active' : '') }}>
                <a href="/users">
                    <i class="icon-layers"></i>
                    <span class="title">Users</span>
                </a>
            </li>
            <li>
                <a href="/profiles">
                    <i class="icon-user"></i>
                    <span class="title">Profiles</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-check"></i>
                    <span class="title">Finance</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-folder"></i>
                    <span class="title">Finance</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#">
                            <i class="icon-settings"></i> Incoming</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-settings"></i> Expences</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
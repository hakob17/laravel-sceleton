</div>

<div class="page-footer">
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

@include('vendor.page.chat-sidebar')

<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript" ></script>
<script src="/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript" ></script>
<script src="/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript" ></script>
<script src="/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript" ></script>
<script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="/assets/admin/layout/scripts/index.js" type="text/javascript"></script>
<script src="/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>

{{--CUSTOM SCRIPTS--}}

<script>
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        Index.init(); // init index page
     
        QuickSidebar.init(); // init quick sidebar

        //Toastr
        $('.toastr').each(function(){
           if($(this).hasClass('status-success')) {
               toastr.success($(this).text(), 'Miracle Max Says', {"positionClass": "toast-top-center", "closeButton": true})
           }

           if($(this).hasClass('status-warning')) {
                toastr.warning($(this).text(), 'Miracle Max Says', {"positionClass": "toast-top-center", "closeButton": true})
           }
        });
    });
</script>

</body>
</html>
@extends('layouts.app')
@section('content')
 <div class="page-content-wrapper">
    <div class="page-content"> 
        <div class="row">
            <div class="col-md-6">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Distance</span>
                            <span class="caption-helper">distance stats...</span>
                        </div>
                        <div class="actions">
                            <a href="#" class="btn btn-circle btn-default btn-sm">
                                <i class="fa fa-pencil"></i> Edit </a>
                            <a href="#" class="btn btn-circle btn-default btn-sm">
                                <i class="fa fa-plus"></i> Add </a>
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="columnLine" class="columnLine"></div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
            <div class="col-md-6">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject bold uppercase font-red-sunglo">Map</span>
                            <span class="caption-helper">flight stats...</span>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-cloud-upload"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-wrench"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                <i class="icon-trash"></i>
                            </a>
                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="animated-pie-chart" class="animated-pie-chart"></div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <div class="portlet light">

            {{--Portlet Title--}}
            <div class="portlet-title">
                <div class="caption font-purple-plum">
                    <span class="caption-subject bold uppercase">User</span>
                    <span class="caption-helper"></span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                </div>
            </div>

            {{--Portlet Body--}}
            <div class="portlet-body">
               <h5>ID: {{$user->id}}</h5>
               <h5>Name: {{$user->name}}</h5>
               <h5>Created: {{$user->created_at}}</h5>
            </div>

        </div>
    </div>
</div>
@endsection

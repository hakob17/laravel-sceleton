@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <div class="portlet light">
            
            {{--Portlet Title--}}
            <div class="portlet-title">
                <div class="caption font-purple-plum">
                    <span class="caption-subject bold uppercase">Users</span>
                    <span class="caption-helper">all fields marked with * are required</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
                </div>
            </div>

            {{--Portlet Body--}}
            <div class="portlet-body">
                <form class="login-form" method="POST" action="{{ url('/users') }}">
                    {{ csrf_field() }}
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label">* Name</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="name" value="{{ old('name') }}" />
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group pull-right">
                            <a href="/users" class="btn default">Cancel</a>
                            <button type="submit" class="btn green">Save</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

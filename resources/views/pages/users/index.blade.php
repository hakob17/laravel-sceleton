@extends('layouts.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content"> 
        <div class="portlet light">
            
            <div class="portlet-title">
                <div class="caption font-purple-plum">
                    <span class="caption-subject theme-font-color bold uppercase">Users</span>
                    <span class="caption-helper">list {{ $users->count()}} of {{ $users->total()}}</span>
                </div>
                <div class="actions">                    
                    <a href="/users/create" class="btn btn-circle red-sunglo btn-sm">
                        <i class="fa fa-plus"></i> Add </a>
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
                    </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="filter">
                    <form class="form-inline" action="/users">
                        <div>
                            <input type="text" class="form-control" name="name" value="{{ array_get($params, 'name', '') }}" placeholder="Name" />
                            <input type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </form>
                </div>

                <table class="table table-hover table-light">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><a href="{{sort_url('users', 'name')}}">Name</a></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($users) > 0)
                            @foreach($users as $i => $user)
                            <tr>
                                <td>{{++$i}}</td>
                                <td><a href="/users/{{$user->id}}">{{$user->name}}</a> </td>
                                <td>
                                    <a href="/users/{{$user->id}}/edit">Edit</a>
                                    <form class="form-inline" method="POST" action="/users/{{$user->id}}">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button type="submit" class="btn btn-link">Delete</button>
                                    </form>

                                 </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="3"> No records found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>

                <div class="pull-right">
                    {{ $users->appends(get_sort_fields())->links() }}
                </div>

                <div class="clearfix"></div>
            </div>
            
        </div>
    </div>
</div>

@if (session('status'))
    <div class="hidden toastr status-success">
        {{ session('status') }}
    </div>
@endif

@endsection

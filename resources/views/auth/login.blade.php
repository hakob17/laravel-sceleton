@extends('layouts.app-auth')
@section('content')
    <div class="content">
        <form class="login-form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="form-title">
                <span class="form-title">Welcome. </span>
                <span class="form-subtitle">Please login</span>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email" value="{{ old('email') }}" />
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary btn-block uppercase">Login</button>
            </div>
            <div class="form-actions">
                <div class="pull-left">
                    <label class="rememberme check">
                        <input type="checkbox" name="remember" />Remember me </label>
                </div>
                <div class="pull-right forget-password-block">
                    <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
            </div>
        </form>
    </div>
@stop
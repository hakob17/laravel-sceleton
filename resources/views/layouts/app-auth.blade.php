@include('vendor.auth.header')

@yield('content')

@include('vendor.auth.footer')
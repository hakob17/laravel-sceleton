## EASY CRUD
### Laravel based project skeleton with build in commands for generate CRUD.
Project Structure
  - There is new Request folder in Http directory where you should have Folder with your model class name, and keep in the folder 4 classes. Lets say your model is User, you shoud have UserReadRequest, UserUpdateRequest, UserStoreRequest and UserDeleteRequest. All this classes extends from Form Request Class of laravel vendor. Example:
```sh
<?php
namespace App\Http\Requests\User;
use Illuminate\Foundation\Http\FormRequest;
class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}
```
  - In the Controlelr use this FormReqest classes for validate request before accessing any action. If validation is not passed laravel will redirect back with errors if request is not ajax, for ajax it reuturn json with 422 error code.  For example:
```sh
public function store(UserStoreRequest){ /*your code here*/}
```



  - Also there is folder Filter where you can find {MODELNAME}Filters.php for each of your models where you can specify fields which allowed to sort and filter. You shoud have methods for each of fields. For Sorting filds sortBy{FieldName} and for filter fields filterBy{FieldName}. Example:
```sh
<?php
namespace App\Http\Filters;

class UserFilter extends AbstractFilter
{
    /**
     * Create a new filter instance.
     */
	public function __construct($query)
	{
		parent::__construct($query);
	}


    /**
     *  Allowed filter fields
     */
	protected $filter  = [
	    'name'
    ];

    /**
     *  Allowed sorting fields
     */
	protected $order  = [
        'name',
        'created'
    ];


    /**
     * Filter by "name" field
     *
     * @param string $value
     */
    protected function filterByName($value = '')
    {
        $this->query->where('name', $value);
    }


    /**
     * Sort by name
     *
     * @param string $dir, ASC or DESC
     */
    protected function sortByName($dir = '')
    {
    	$this->query->orderBy('name', $dir);
    }

    /**
     * Sort by created_at
     *
     * @param string $dir, ASC or DESC
     */
    protected function sortByCreated($dir = '')
    {
    	$this->query->orderBy('created_at', $dir);
    }
}
```

### Generate Files
Simply Run 

```sh
$ php artisan make:crud {MODEL_NAME}
```

It will create migrate using your model name, fill all fields you need and run

```sh
$ php artisan migrate
```
Thats all. Type in prowser www.your_project/Model_name for accessing views


### Todos
 - Write Tests

License
----

MIT


**Free Software, Hell Yeah!**
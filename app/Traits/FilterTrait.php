<?php
namespace App\Traits;

trait FilterTrait
{

    public function scopeGetAll($query, $attrs = [], $perPage = PAGE_LIMIT)
    {
        return $query->filter($attrs)->paginate($perPage);
    }


    public function scopeFilter($query, $filters = [])
    {
        $model_name = class_basename($query->getModel());

        $filter_class = 'App\\Http\\Filters\\' . ucfirst($model_name) . 'Filter';

        if (class_exists($filter_class)) {
            $filter_object = new $filter_class($query);
            
            //Filtering
            foreach ($filters as $filterName => $filterValue) {

                if (empty($filterValue)) {
                    continue;
                }

                $filter_object->_callFilter($filterName, $filterValue);             
            }

            //Sorting
            $sort_field = array_get($filters, 'sort', 'created_at');
            $dir = array_get($filters, 'dir', 'DESC');

            $filter_object->_callSorter($sort_field, $dir);
        }
    }
}
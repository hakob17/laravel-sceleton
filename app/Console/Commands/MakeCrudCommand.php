<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeCrudCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:crud';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $name = Str::studly(class_basename($this->argument('name')));
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));

         $this->call('make:requests', [
             'name' => "{$name}DeleteRequest",
             'path' => $name
         ]);

         $this->call('make:requests', [
             'name' => "{$name}ReadRequest",
             'path' => $name
         ]);

         $this->call('make:requests', [
             'name' => "{$name}StoreRequest",
             'path' => $name
         ]);

         $this->call('make:requests', [
             'name' => "{$name}UpdateRequest",
             'path' => $name
         ]);


         $this->call('make:models', [
             'name' => $name
         ]);

         $this->call('make:filter', [
             'name' => "{$name}Filter",
         ]);

        $this->call('make:controllers', [
            'name' => $name,               
        ]);

        $this->call('make:views', [
            'name'   => 'index', 
            'folder' => $name           
        ]);

        $this->call('make:views', [
            'name'   => 'create', 
            'folder' => $name           
        ]);

         $this->call('make:views', [
            'name'   => 'show', 
            'folder' => $name           
        ]);

        $this->call('make:views', [
            'name'   => 'edit', 
            'folder' => $name           
        ]);

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);

        //Append route
        $route_file = $this->laravel['path.base'] . '/routes/web.php';
        $route_txt = "Route::resource('/" . strtolower($name) ."', '{$name}Controller');";

        file_put_contents($route_file,  $route_txt . PHP_EOL , FILE_APPEND | LOCK_EX);       

    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/readRequest.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }  
}

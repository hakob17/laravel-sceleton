<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeViewCommand extends GeneratorCommand
{
     /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create View';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:views {name} {folder}';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';
   
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {       
        $name = $this->parseName($this->getNameInput());

        $path = $this->getPath($name);


        if ($this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildFile($name));

        $this->info($this->type.' created successfully.');        
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    private function buildFile($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceVariables($stub, $name);
    }

    

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceVariables(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name); 
        $folder = $this->argument('folder');     
       
        $stub  = str_replace('DummyViewPath', strtolower($folder), $stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $name = strtolower($this->argument('name'));       

        return __DIR__."/stubs/{$name}.stub";
    }

     /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace($this->laravel->getNamespace(), '', $name);
        
        $folder = strtolower($this->argument('folder'));

        return $this->laravel['path.base']."/resources/views/pages/{$folder}/".str_replace('\\', '/', $name).'.blade.php';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    // protected function getDefaultNamespace($rootNamespace)
    // {   
    //    // $path = $this->argument('path');
    //    // $name = $this->argument('name');

    //     //$path = empty($path) ? '' : '\\' . $path;
       
    //   // return '\resources\views\pages';
        
    //     //return '\resources\views\pages' .$name;
      
    // }

}

<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeControllersCommand extends GeneratorCommand
{
     /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Controller';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:controllers {name}';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'File';
   
    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {       
        $name = $this->parseName($this->getNameInput());

        $path = $this->getPath($name.'Controller');

        if ($this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');        
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceNamespace($stub, $name)->replaceFolder($stub, $name)->replaceRequests($stub, $name)->replaceClass($stub, $name . 'Controller');
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceRequests(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        $stub = str_replace('DummyReadRequest', "{$class}ReadRequest", $stub);
        $stub = str_replace('DummyStoreRequest', "{$class}StoreRequest", $stub);
        $stub = str_replace('DummyUpdateRequest', "{$class}UpdateRequest", $stub);
        $stub = str_replace('DummyDeleteRequest', "{$class}DeleteRequest", $stub);

        return $this;
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceFolder(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        //dd();
        $stub = str_replace('DummyFolder',  $class, $stub);
        $stub = str_replace('DummyViewPath', strtolower($class), $stub);

        return $this;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/controller.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {   
        return $rootNamespace . '\Http\Controllers';
    }
}

<?php
namespace App\Http\Filters;

class UserFilter extends AbstractFilter
{
    /**
     * Create a new filter instance.
     */
	public function __construct($query)
	{
		parent::__construct($query);
	}


    /**
     *  Allowed filter fields
     */
	protected $filter  = [
	    'name'
    ];

    /**
     *  Allowed sorting fields
     */
	protected $order  = [
        'name',
        'created'
    ];


    /**
     * Filter by "name" field
     *
     * @param string $value
     */
    protected function filterByName($value = '')
    {
        $this->query->where('name', $value);
    }


    /**
     * Sort by name
     *
     * @param string $dir, ASC or DESC
     */
    protected function sortByName($dir = '')
    {
    	$this->query->orderBy('name', $dir);
    }

    /**
     * Sort by created_at
     *
     * @param string $dir, ASC or DESC
     */
    protected function sortByCreated($dir = '')
    {
    	$this->query->orderBy('created_at', $dir);
    }
}